# -*- coding: utf-8 -*-

db.define_table('posts',
                Field('userId','integer', default=auth.user_id, writable=False, readable=False),
                Field('textual', 'text', label=T('Texto') ),
                Field('files', 'string', label=T('Ficheiros') ),
                Field('thumbnail', 'upload', label=T('Imagem de Capa') ),
                Field('privacy', 'string', label=T('Privacidade') ),
                Field('dataReg', 'datetime', default=request.now, writable=False, readable=False, label=T('Data de Criação') )
                )

db.posts.privacy.requires = IS_IN_SET([T('Todos'), T('Grupo'), T('Utilizador')], zero=T('Escolha uma opção...'), error_message=T('Escolha uma opção') )
