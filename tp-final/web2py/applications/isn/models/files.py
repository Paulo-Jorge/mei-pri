# -*- coding: utf-8 -*-

db.define_table('files',
                Field('userId','integer', default=auth.user_id, writable=False, readable=False),
                Field('fileType', 'string', label=T('Tipo') ),
                Field('fileUp', 'upload', label=T('Ficheiro') ),
                Field('fileLink', 'string', label=T('URL Ficheiro') ),
                Field('description', 'string', label=T('Descrição') ),
                Field('dataReg', 'datetime', default=request.now, writable=False, readable=False, label=T('Data de Criação') )
                )

db.files.fileType.requires = IS_IN_SET([T('PDF'), T('Áudio'), T('Vídeo'), T('Imagem'), T('Texto'), T('Comprimido'), T('Outro')], zero=T('Escolha uma opção...'), error_message=T('Escolha uma opção') )
