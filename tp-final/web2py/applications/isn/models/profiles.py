# -*- coding: utf-8 -*-

db.define_table('profiles',
                Field('userId','integer', default=auth.user_id, writable=False, readable=False),
                Field('userType', 'string', label=T('Tipo') ),
                Field('userDegree', 'string', label=T('Grau Académico') ),
                #Field('escola', 'string', label=T('Universidade') ),
                Field('city', 'string', length=128, label=T('Cidade de residência') ),
                Field('dataReg', 'datetime', default=request.now, writable=False, readable=False, label=T('Data de Criação') )
                )

db.profiles.userType.requires = IS_IN_SET([T('Aluno'), T('Professor')], zero=T('Escolha uma opção...'), error_message=T('Escolha uma opção') )
db.profiles.userDegree.requires = IS_IN_SET([T('Licenciatura'), T('Mestrado'), T('Doutoramento'), T('Agregação'), T('Outro')], zero=T('Escolha uma opção...'), error_message=T('Escolha uma opção') )
