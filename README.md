**MEI 2019/2021 - Processamento e Representação de Informação**

Paulo Jorge Pereira Martins | Nº PG17918

*paulo.jorge.pm@gmail.com | pg17018@alunos.uminho.pt | [Website](http://paulojorgepm.net)*

---

## Trabalhos práticos:

1. Trabalho 1: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tpc1/) | [Demo Alojado](http://ceh.ilch.uminho.pt/temp/catlandia/)

2. Trabalho 2: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tp2/)

3. Trabalho 3: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tpc3/)

4. Trabalho 4: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tpc4/)

5. Trabalho 5: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tpc5/)

6. Trabalho 6: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tpc6/)

7. Trabalho 7: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tpc7/)

8. Trabalho 8: [Repositório](https://bitbucket.org/Paulo-Jorge/mei-pri/src/master/tpc8/)

---

