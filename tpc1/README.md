**1º Trabalho Individual - MEI 2019/2021 - PRI**

Task: criar uma página HTML com a temática "gatos". Prazo de entrega: 2019/09/30

Demo online a correr [aqui](http://ceh.ilch.uminho.pt/temp/tpc1/)

*Requisitos para correr o repositório: nenhum, basta lançar o index.html num browser moderno (apenas usa HTML5/CSS e JS/JQUERY). Não testado em resoluções mobile, apenas testado em 1980px + Chrome/Firefox recentes*

Aluno: Paulo Jorge Pereira Martins | Nº PG17918 | E-mail: paulo.jorge.pm@ilch.uminho.pt

---

