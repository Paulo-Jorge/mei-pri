<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8"/>

    <xsl:template match="/">
        <xsl:result-document href="website/index.html">
            <html>
                <head>
                    <title>Arqueossítios</title>
                    <meta charset="UTF-8"/>
                    <link rel="stylesheet" type="text/css" href="../static/css/style.css"/>
                </head>
                <body>
                    <div id="content">
                        <div id="left">
                            <ul class="arq">
                                <xsl:apply-templates/>
                            </ul>
                        </div>
                        <div id="right">
                            <h1>Arqueossítios</h1>
                            <iframe id="iframe_main" name="iframe_main" srcdoc="&#x0003C;b&#x0003E;Selecione uma ficha para
                                visualizar...&#x0003C;/b&#x0003E; &#x0003C;br /&#x0003E; &#x0003C;br /&#x0003E; &#x0003C;img src='../static/images/search.png'&#x0003E;">O seu browser não suporta iframes... Por favor atualizei para uma versão mais recente.</iframe>
                        </div>
                    </div>
                </body>
            </html>
        </xsl:result-document>

        <xsl:apply-templates mode="individual"/>
    </xsl:template>

    <xsl:template match="ARQELEM" mode="individual">
        <xsl:result-document href="website/{generate-id()}.html">
            <html>
                <head>
                    <title>Arqueossítio: Página Individual</title>
                    <meta charset="UTF-8"/>
                    <link rel="stylesheet" type="text/css" href="../static/css/style.css"/>
                </head>
                <body>
                    <h1>
                        <xsl:value-of select="IDENTI"/>
                    </h1>
                    <table>
                        <tr>
                            <th>Autor:</th>
                            <td>
                                <xsl:value-of select="AUTOR"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Data:</th>
                            <td>
                                <xsl:value-of select="DATA"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Contexto:</th>
                            <td>
                                <xsl:value-of select="CRONO"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Lugar:</th>
                            <td>
                                <xsl:value-of select="LUGAR"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Freguesia:</th>
                            <td>
                                <xsl:value-of select="FREGUE"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Concelho:</th>
                            <td>
                                <xsl:value-of select="CONCEL"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Código:</th>
                            <td>
                                <xsl:value-of select="CODADM"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Latitude:</th>
                            <td>
                                <xsl:value-of select="LATITU"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Longitude:</th>
                            <td>
                                <xsl:value-of select="LONGIT"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Altitude:</th>
                            <td>
                                <xsl:value-of select="ALTITU"/>
                            </td>
                        </tr>
                    </table>

                    <hr/>

                    <div class="block">
                        <h2>Acesso:</h2>
                        <xsl:apply-templates select="ACESSO"/>
                    </div>

                    <div class="block">
                        <h2>Enquadramento:</h2>
                        <xsl:apply-templates select="QUADRO"/>
                    </div>

                    <div class="block">
                        <h2>Trabalhos arqueológicos:</h2>
                        <xsl:apply-templates select="TRAARQ"/>
                    </div>

                    <div class="block">
                        <h2>Descrição:</h2>
                        <xsl:apply-templates select="DESARQ"/>
                    </div>

                    <div class="block">
                        <h2>Interpretação:</h2>
                        <xsl:apply-templates select="INTERP"/>
                    </div>

                    <div class="block">
                        <h2>Bibliografia:</h2>
                        <ol>
                        <xsl:for-each select="BIBLIO">
                            <li><xsl:apply-templates select="."/></li>
                        </xsl:for-each>
                        </ol>
                    </div>

                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="ACESSO">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="QUADRO">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="TRAARQ">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="DESARQ">
        <xsl:apply-templates />
    </xsl:template>
       
    <xsl:template match="INTERP">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="BIBLIO">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="LIGA">
        <b><span style="color:red;"><xsl:value-of select="."/></span></b>
    </xsl:template>

    <xsl:template match="ARQELEM">
        <li id="{generate-id()}">
            <a href="{generate-id()}.html" target="iframe_main">
                <xsl:value-of select="IDENTI"/>
            </a>
        </li>
    </xsl:template>

</xsl:stylesheet>
