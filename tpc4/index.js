var http = require('http');
var fs = require('fs');
var pug = require('pug');

var dbPath = "data";

var myserver = http.createServer(function (req, res) {

    const baseUrl = 'http://' + req.headers.host;
	const method = req.method;
    const url = new URL(req.url, baseUrl);
    const urlPathname = url.pathname;
    const urlParams = url.searchParams;
    


	console.log(req.method + ' | req.url: ' + req.url + ' | Full: ' + url);
	
	if (req.method == 'GET') {
        pageParams = req.url.split('/');
        file = pageParams[pageParams.length - 1];
        base = pageParams[pageParams.length - 2];
	    
	/*if (urlParams.has('file')) {
	    const file = urlParams.get('file');
	    console.log(file);
	}*/
	console.log(urlParams);
	
		if (req.url == '/') {
		
            res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'})
            res.write(pug.renderFile('index.pug'))
            res.end()
		}
	 //&& !isNaN(parseInt(file)) => confirma se a terminação do url é um nº
		else if (base=='musica' && !isNaN(parseInt(file)) ) {
			fs.readFile('data/doc'+ file +'.xml', (erro, dados) =>{
				if(!erro) {
					res.writeHead(200, {'Content-Type': 'text/xml'});
					res.write(dados);
					console.log("XML OKKKKKKKK");
				}
				else {
					res.writeHead(200, {'Content-Type': 'text/plain'});
					res.write('Erro na leitura do XML...');
				}
				res.end(); 
			})
		}
		
		else if(file == 'doc2html.xsl'){
            fs.readFile('doc2html.xsl', (erro, dados)=>{
                if(!erro){
                console.log("XLS OKKKKKKK");
                    res.writeHead(200, {'Content-Type':'text/xml'}) 
                    res.write(dados);
                }
                else {
                    res.writeHead(200, {'Content-Type':'text/plain'}) 
                    res.write('Erro na leitura do XLS...')
                }
                res.end()  
            });
        }
		
		else if (file == 'style.css') {
			fs.readFile('static/css/style.css', (erro, dados) =>{
				if(!erro) {
					res.writeHead(200, {'Content-Type': 'text/css'})
					res.write(dados)
				}
				else {
					res.writeHead(200, {'Content-Type': 'text/plain'})
					res.write('Erro na leitura do CSS...')
				}
				res.end() 
			})
		}
	
	}
})


myserver.listen(8080); 

console.log('Seridor à escuta na porta 8080...');


