<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    
    <xsl:template match="doc">
        <html>
            <head>
                <title>TPC4</title>
                <meta charset="UTF-8" />
                <link rel="stylesheet" type="text/css" href="static/css/style.css" />
                <style type="text/css">
                    a {color:#00d4ff;}
                    a:hover {background:black;}
                </style>
            </head>
            <body>
            <!-- style="bakground:beige; background:linear-gradient(beige, orange);background:repeating-radial-gradient(circle at 0 0, #eee, #ccc 50px);" style="text-align:center;margin:30px 0px;font-size:3em;"-->
            <div style="padding:10px 5px 50px 50px;">
                        <ul>
                        <li><b>Província: </b> <xsl:value-of select="prov"/></li>
                        <li><b>Localidade: </b> <xsl:value-of select="local"/></li>
                        <li><b>Título: </b> <xsl:value-of select="tit"/></li>
                        <li><b>Músico: </b> <xsl:value-of select="musico"/></li>
                        <li><b>Ficheiro: </b> <xsl:value-of select="file"/> (<xsl:value-of select="file/@t"/>)</li>
                        <li><b>Duração: </b> <xsl:value-of select="duracao"/></li>
                        </ul>
            </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
