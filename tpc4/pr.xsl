<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">
    
    <xsl:output method="xhtml" indent="yes" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <xsl:result-document href="pr.html" >
            <html>
                <head>
                    <title>TPC4</title>
                    <meta charset="UTF-8" />
                    <style type="text/css">
                        a {color:#00d4ff;}
                        a:hover {background:black;}
                    </style>
                </head>
                <body style="bakground:beige; background:linear-gradient(beige, orange);background:repeating-radial-gradient(circle at 0 0, #eee, #ccc 50px);">
                    <h1 style="text-align:center;margin:30px 0px;font-size:3em;"><span style="background:yellow;">Project Record</span></h1>
                    <xsl:apply-templates />
                </body>
            </html>
        </xsl:result-document>
    </xsl:template> 
    
    <xsl:template match="meta">
        <div style="padding:10px 20px;background:rgba(200,40,0,0.8);box-shadow:2px 1px #333333;color:#ffffff;">
        <p><b>Key: </b> <xsl:value-of select="./keyname"/></p>
        <p><b>Title: </b> <xsl:value-of select="./title"/></p>
        <xsl:if test="subtitle != ''"><p><b>Subtile: </b> <xsl:value-of select="./subtitle"/></p></xsl:if>
        <p><b>BEGIN DATE: </b><xsl:value-of select="./bdate"/> | <b>END DATE:</b> <xsl:value-of select="./edate"/></p>
        <p><b>SUPERVISOR: </b><xsl:value-of select="./supervisor/name"/> [<a href='mailto:{./supervisor/email}'>E-MAIL</a> | <a href='{./supervisor/homepage}' target="_blank">HOMPAGE</a>]</p>
        </div>
        <br />
        <hr />
    </xsl:template>

    <xsl:template match="workteam">
        <h2>Membros:</h2>
        <xsl:apply-templates />
        
        <br />
        <hr />
    </xsl:template>

    <xsl:template match="workteam/member">
        <div style="display:inline-block;border:1px solid #333;padding:0px 25px 0px 170px;background:rgba(200,40,0,0.8) url({photo/@path}) no-repeat;box-shadow:2px 1px #333333;color:#ffffff;margin:10px 20px 10px 0px;min-height:193px;">
        <p style="margin-top:30px;"><b>Nome: </b><xsl:value-of select="name"/></p>
        <p><b>ID: </b><xsl:value-of select="identifier"/></p>
        <p><b>E-mail: </b><a href="mailto:{./email}"><xsl:value-of select="email"/></a></p>
        </div>        
    </xsl:template>


    <xsl:template match="abstract">
        <h2>Abstract:</h2>
        <div style="padding:10px 20px;background:rgba(200,40,0,0.8);box-shadow:2px 1px #333333;color:#ffffff;">
        <xsl:apply-templates />
        </div>
        <!--<xsl:copy-of select="." />-->
        <br />
        <hr />
    </xsl:template>
    
    <xsl:template match="p">
        <p><xsl:apply-templates /></p>
    </xsl:template>
    
    <xsl:template match="xref">
        <a href="{./@url}" target="_blank"><xsl:value-of select="."/></a>
    </xsl:template>

    <xsl:template match="b">
        <b><xsl:value-of select="."/></b>
    </xsl:template>

    <xsl:template match="i">
        <i><xsl:value-of select="."/></i>
    </xsl:template>

    <xsl:template match="u">
        <u><xsl:value-of select="."/></u>
    </xsl:template>

    <xsl:template match="deliverables">
        <h2>Deliverables:</h2>
        <div style="display:inline-block;margin-right:20px;padding:10px 50px;background:rgba(200,40,0,0.8);box-shadow:2px 1px #333333;color:#ffffff;">
        <ol>
        <xsl:apply-templates />
        </ol>
        </div>
        <br />
        <hr />
    </xsl:template>
    
    <xsl:template match="deliverable">
        <li><a href="{./@path}" target="_blank"><xsl:value-of select="."/></a></li>
    </xsl:template>
    
</xsl:stylesheet>
