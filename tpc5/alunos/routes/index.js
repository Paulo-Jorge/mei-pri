var express = require('express');
var router = express.Router();
var jsonfile = require('jsonfile');
var nanoid = require('nanoid');

var myBD = __dirname + "/../data/alunos.json";
console.log(myBD);

router.get('/', function(req, res, next) {
  res.redirect("/alunos");
});

router.get('/alunos', function(req, res, next) {
  jsonfile.readFile(myBD, (erro, dados) => {
    if(!erro){
        res.render('index', {lista: dados})
    }
    else{
        res.render('error', {error: erro})
    }
  });
});

router.get('/alunos/:idAluno', function(req, res, next) {
  var id = req.params.idAluno;
  jsonfile.readFile(myBD, (erro, alunos)=>{
    if(!erro){
      var index = alunos.findIndex(c => c.idAluno == id);
      if(index > -1){
        var dados = alunos.splice(index, 1);
        res.render('notas', {lista: dados, idALuno: id});
      }
    }
    else{
        res.render('error', {error: erro});
    }
  })
})

//Adicionar aluno
router.post('/alunos', function(req, res) {
    var registo = req.body;
    registo['idAluno'] = nanoid();
    registo["notas"] = [];
    jsonfile.readFile(myBD, (erro, alunos)=>{
        if(!erro){
            alunos.push(registo);
            jsonfile.writeFile(myBD, alunos, erro => {
                if(erro) console.log(erro);
                else console.log('Aluno gravado com sucesso.');
            })
        }
        res.render('index', {lista: alunos});
    });
});

//Adicionar nota a um aluno
router.post('/alunos/:idAluno/notas', function(req, res) {
    var id = req.params.idAluno;
    var registo = req.body;
    registo['idNota'] = nanoid();

    jsonfile.readFile(myBD, (erro, alunos)=>{
      if(!erro){
        var index = alunos.findIndex(c => c.idAluno == id);
        if(index > -1){
          alunos[index]["notas"].push(registo);

            jsonfile.writeFile(myBD, alunos, erro => {
                if(erro) console.log(erro)
                else console.log('Nota gravada com sucesso.')
            })
        }
        res.redirect("/alunos/"+id);
      }
      else{
          res.render('error', {error: erro});
      }
      //res.render('notas', {lista: alunos[index]});
    })
});

//Apagar aluno
router.delete('/alunos/:idAluno', function(req, res) {
  var id = req.params.idAluno;
  jsonfile.readFile(myBD, (erro, alunos)=>{
    if(!erro){
      var index = alunos.findIndex(c => c.idAluno == id);
      if(index > -1){
        alunos.splice(index, 1);
        jsonfile.writeFile(myBD, alunos, erro => {
          if(erro) console.log(erro)
          else console.log('Aluno removido com sucesso.')
        })
      }
    }
    res.render('index', {lista: alunos});
  })
})

//Apagar nota de aluno
router.delete('/alunos/:idAluno/notas/:indicador', function(req, res) {
  var id = req.params.idAluno;
  var indicador = req.params.indicador;

    jsonfile.readFile(myBD, (erro, alunos)=>{
      if(!erro){
        var index = alunos.findIndex(c => c.idAluno == id);
        if(index > -1){
          var indexNota = alunos[index]["notas"].findIndex(c => c.idNota == indicador);
          alunos[index]["notas"].splice(indexNota, 1);

          jsonfile.writeFile(myBD, alunos, erro => {
            if(erro) console.log(erro)
            else console.log('Nota removida com sucesso.')
          })
        }
        //303 porque o redirect de delete enviava outro delete. explicação aqui: 
        //https://stackoverflow.com/questions/33214717/why-post-redirects-to-get-and-put-redirects-to-put
        res.redirect(303, "/alunos/"+id);
      }
      else{
          res.render('error', {error: erro});
      }
      //res.render('notas', {lista: alunos});
    })
})

module.exports = router;
