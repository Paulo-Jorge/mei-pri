var express = require('express');
var router = express.Router();
var jsonfile = require('jsonfile');
const axios = require('axios');

apikey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Nzg4NjAwNTQsImV4cCI6MTU4MTQ1MjA1NH0.HIlH4_Ao6504qaLhhbZ2_OtDzaZaG5FeYy-Yc2d9lwQ";
tipologias = "http://clav-api.dglab.gov.pt/api/tipologias?apikey=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Nzg4NjAwNTQsImV4cCI6MTU4MTQ1MjA1NH0.HIlH4_Ao6504qaLhhbZ2_OtDzaZaG5FeYy-Yc2d9lwQ"

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/

router.get('/', function(req, res, next) {
    axios.get(tipologias)

    .then(dados => { res.render('tipologias', {lista: dados.data} ) } )
    .catch(erro => res.send('Erro: '+ erro))
    
  /*jsonfile.readFile(db, (erro, dados) => {
    if(!erro){
        res.render('entidades', {lista: dados})
    }
    else{
        res.render('error', {error: erro})
    }
  });*/
});

router.get('/tipologia/:idTipo', function(req, res, next) {
    url = "http://clav-api.dglab.gov.pt/api/tipologias/" + req.params.idTipo + "?apikey=" + apikey;

    axios.get(url)
    .then(dados => { res.render('tipologia', {lista: dados.data} ) } )
    .catch(erro => res.send('Erro: '+ erro))
});

router.get('/dono/:idTipo', function(req, res, next) {
    url = "http://clav-api.dglab.gov.pt/api/tipologias/" + req.params.idTipo + "/intervencao/dono?apikey=" + apikey;

    axios.get(url)
    .then(dados => { res.render('dono', {lista: dados.data} ) } )
    .catch(erro => res.send('Erro: '+ erro))
});

module.exports = router;
