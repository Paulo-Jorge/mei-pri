function apagarAluno(idAluno){
    console.log('Vou apagar /alunos/: ' + idAluno)
    axios.delete('/alunos/' + idAluno)
        .then(response => window.location.assign('/alunos'))
        .catch(error => console.log(error))
}

function apagarNota(idAluno, idNota){
    console.log('Vou apagar: /alunos/' + idAluno + '/notas/' + idNota)
    axios.delete('/alunos/' + idAluno + '/notas/' + idNota)
        .then(response => window.location.assign('/alunos/'+ idAluno))
        .catch(error => console.log(error))
}

