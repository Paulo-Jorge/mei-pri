

Quantos Tipologias estão catalogadas?
R: 76

Quantos Entidades pertencem à Tipologia tip_FSS?
R: 1

A que tipologias pertence a entidade ent_SEF?
R: Às tipologias tip_AP e tip_FSS
JSON: [{"sigla":"AP","designacao":"Administração Pública","id":"tip_AP"},{"sigla":"FSS","designacao":"Forças e serviços de segurança","id":"tip_FSS"}]

Em que processos a tipologia tip_FA participa como Comunicador?
R: Pertence aos seguintes id's:
[
    {
        "codigo": "150.30.001",
        "titulo": "Produção e intercâmbio de informação estratégica para segurança interna e defesa",
        "id": "c150.30.001"
    },
    {
        "codigo": "550.10.004",
        "titulo": "Apoio de forças de segurança a diligências",
        "id": "c550.10.004"
    },
    {
        "codigo": "550.10.005",
        "titulo": "Segurança de infraestruturas estratégicas",
        "id": "c550.10.005"
    },
    {
        "codigo": "550.10.600",
        "titulo": "Reposição da ordem e gestão de incidentes de segurança",
        "id": "c550.10.600"
    },
    {
        "codigo": "550.20.003",
        "titulo": "Realização de exercícios e simulacros",
        "id": "c550.20.003"
    },
    {
        "codigo": "550.30.001",
        "titulo": "Participação em operações de defesa convencional do território nacional",
        "id": "c550.30.001"
    },
    {
        "codigo": "550.30.002",
        "titulo": "Ciberdefesa",
        "id": "c550.30.002"
    },
    {
        "codigo": "550.30.004",
        "titulo": "Evacuação de cidadãos nacionais em áreas de tensão",
        "id": "c550.30.004"
    },
    {
        "codigo": "550.30.005",
        "titulo": "Extração e proteção de contingentes e Forças Nacionais destacadas",
        "id": "c550.30.005"
    },
    {
        "codigo": "550.30.200",
        "titulo": "Defesa do território das nações aliadas",
        "id": "c550.30.200"
    },
    {
        "codigo": "550.30.400",
        "titulo": "Vigilância dos espaços sob soberania e jurisdição nacional, da circulação e segurança das linhas de comunicação",
        "id": "c550.30.400"
    },
    {
        "codigo": "550.30.600",
        "titulo": "Participação em operações de resposta a crises",
        "id": "c550.30.600"
    },
    {
        "codigo": "550.30.601",
        "titulo": "Participação em missões de apoio à paz",
        "id": "c550.30.601"
    },
    {
        "codigo": "710.20.004",
        "titulo": "Entrega de restos mortais",
        "id": "c710.20.004"
    },
    {
        "codigo": "710.20.600",
        "titulo": "Depósito de urnas",
        "id": "c710.20.600"
    },
    {
        "codigo": "710.20.601",
        "titulo": "Trasladação de cadáveres e ossadas",
        "id": "c710.20.601"
    },
    {
        "codigo": "750.20.601",
        "titulo": "Realização de atividades de formação e treino animal",
        "id": "c750.20.601"
    },
    {
        "codigo": "900.10.505",
        "titulo": "Organização e participação em cerimónia religiosa",
        "id": "c900.10.505"
    },
    {
        "codigo": "950.30.001",
        "titulo": "Reconhecimento por mérito e serviços prestados",
        "id": "c950.30.001"
    },
    {
        "codigo": "550.30.006",
        "titulo": "Realização de exercícios militares",
        "id": "c550.30.006"
    },
    {
        "codigo": "550.20.500",
        "titulo": "Ação de proteção e socorro",
        "id": "c550.20.500"
    }
]
