var Db = require('../models/db');

module.exports.listar = () => {
    return Db
        .find().select('id title year type -_id')
        .exec()
}

module.exports.consultar = id => {
    return Db
        .findOne({id: id})
        .exec()
}

module.exports.tipos = () => {
    return Db
        .find().select('type -_id')
        .distinct('type')
        .exec()
}

module.exports.tipoYYY = tipo => {
    return Db
        .find({type : tipo})
        .exec()
}

module.exports.tipoYYYanoAAAA = (tipo, year) => {
    return Db
        .find({type : tipo, year:{$gt:year}})
        .exec()
}

module.exports.autores = () => {
    return Db
    	//A contagem dos autores é um extra, não era pedido
    	.aggregate([ { $unwind: "$authors" }, {$group:{_id: "$authors", numAuthors: {$sum: 1}}}, {$sort: {_id:1}} ])
        .exec()
}

module.exports.pubsAutores = (autor) => {
    return Db
    	.find({ authors: autor })
        .exec()
}