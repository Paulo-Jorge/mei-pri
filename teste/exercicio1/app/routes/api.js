var express = require('express');
var router = express.Router();

var Publications = require('../controllers/pubs');

router.get('/pubs', function(req, res, next) {

if(req.query.type) {
		if(req.query.year) {
		  Publications.tipoYYYanoAAAA(req.query.type, req.query.year)
		    .then(dados => res.jsonp(dados))
		    .catch(erro => res.status(500).jsonp(erro))
		}
		else {
		  Publications.tipoYYY(req.query.type)
		    .then(dados => res.jsonp(dados))
		    .catch(erro => res.status(500).jsonp(erro))
		}
	}
else if (req.query.autor) {
	  Publications.pubsAutores(req.query.autor)
	    .then(dados => res.jsonp(dados))
	    .catch(erro => res.status(500).jsonp(erro))
	}
else {
	  Publications.listar()
	    .then(dados => res.jsonp(dados))
	    .catch(erro => res.status(500).jsonp(erro))
	}

});

router.get('/pubs/:idPub', function(req, res) {
  Publications.consultar(req.params.idPub)
    .then(dados => res.jsonp(dados))
    .catch(erro => res.status(500).jsonp(erro))
})

router.get('/pubs', function(req, res) {
  Publications.tipos()
    .then(dados => res.jsonp(dados))
    .catch(erro => res.status(500).jsonp(erro))
})

router.get('/types', function(req, res) {
  Publications.tipos()
    .then(dados => res.jsonp(dados))
    .catch(erro => res.status(500).jsonp(erro))
})

router.get('/autores', function(req, res) {
  Publications.autores()
    .then(dados => res.jsonp(dados))
    .catch(erro => res.status(500).jsonp(erro))
})

module.exports = router;
