$(function () {
    var cont = 1
    
    $("#mais1").click(e=>{
        e.preventDefault()
        cont++
        var campo = $('<div></div>',{class: 'w3-container', id: 'f'+cont})
        var desc = $('<div></div>',{class: 'w3-cell-row', id: 'desc'+cont})
        var descLabel = $('<label class="w3-cell">Descrição:</label>')
        var descInput = $('<input/>',{class: 'w3-input w3-cell', type: 'text', name:"desc"})
        var ficheiro = $('<div></div>',{class: 'w3-cell-row', id: 'ficheiro'+cont})
        var ficheiroLabel = $('<label class="w3-cell">Ficheiro:</label>')
        var ficheiroInput = $('<input/>',{class: 'w3-input w3-cell', type: 'file', name:"ficheiro"})
        $("#lista").append(campo)
        $("#f" + cont).append(desc)
        $("#desc" + cont).append(descLabel,descInput)
        $("#f" + cont).append(ficheiro)
        $("#ficheiro" + cont).append(ficheiroLabel,ficheiroInput)
    })
})

function show_me(linha, f){
var ficheiro = $('<h1>Nome: ' + f.name + '</h1><p>Tipo: ' + f.mimetype + '<br>Tamanho: ' + f.size + '</p>')

    if(f.mimetype == 'image/png' || f.mimetype == 'image/jpeg') {
    
        $('#imagemdisplay').empty()
        $('#imagemdisplay').append(ficheiro)
        $('#imagemdisplay').load(f.path)
        $('#imagemdisplay').append($('<img src="images/' + f.name + '" width="100%"/>'))
        $('#imagemdisplay').modal()
    }

    else {
        $('#display').empty()
        $('#display').append(ficheiro)
        $('#display').modal()
    }
    
}
