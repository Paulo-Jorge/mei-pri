var Obra = require('../models/obras');

module.exports.listarTodos = () => {
    return Obra
        .find()
        //.sort({$natural:-1})
        //.sort()
        .exec()
}

module.exports.listarPeriodo = (periodo) => {
    return Obra
        .find({periodo: periodo})
        .exec()
}

module.exports.listarAno = (ano) => {
    return Obra
        .find({anoCriacao : ano})
        //.find({anoCriacao: { $eq: ano } })
        .exec()
}

module.exports.listarCompositor = (compositor) => {
    return Obra
        .find({compositor : compositor})
        .exec()
}
