var Obras = require('../models/obras');

module.exports.listarCompositor = (compositor) => {
    return Obras
        .find({compositor : compositor})
        .exec()
}

module.exports.listarCompositores = () => {
    return Obras
        .find()
        .distinct('compositor')
        //.select(Obras.compositor)
        //.sort()
        .exec()
}
