var express = require('express');
var router = express.Router();

var Obras = require('../controllers/compositores');

router.get('/', function(req, res) {
        Obras.listarCompositores()
        .then(dados => res.jsonp(dados) )
        .catch(erro => res.status(500).jsonp(erro))
})

router.get('/:nome', function(req, res) {
    Obras.listarCompositor(req.param.nome)
    .then(dados => res.jsonp(dados))
    .catch(erro => res.status(500).jsonp(erro))
})

module.exports = router;
